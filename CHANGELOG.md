## [2.8.1](https://gitlab.gwdg.de/dariah-de/dariah-de-digilib-services/compare/v2.8.0...v2.8.1) (2024-05-27)


### Bug Fixes

* add bind-api to deps ([d4e593a](https://gitlab.gwdg.de/dariah-de/dariah-de-digilib-services/commit/d4e593a098e972b6fc012b45830f8508fa22687b))
* fix some deps, hopefully ([5bcd601](https://gitlab.gwdg.de/dariah-de/dariah-de-digilib-services/commit/5bcd60109c3ace039a193062c1939b2aa1fc8947))
* remove spring beans dep again ([b024a88](https://gitlab.gwdg.de/dariah-de/dariah-de-digilib-services/commit/b024a88664cb081f8afc51d20bf48b341cc2f555))

# [2.8.0](https://gitlab.gwdg.de/dariah-de/dariah-de-digilib-services/compare/v2.7.2...v2.8.0) (2024-05-23)


### Bug Fixes

* add mp client to service ([d32ebf2](https://gitlab.gwdg.de/dariah-de/dariah-de-digilib-services/commit/d32ebf22db9663f84085dc6ab80944c44739a19f))
* add tgcrud and tgauth clients ([bb24c9c](https://gitlab.gwdg.de/dariah-de/dariah-de-digilib-services/commit/bb24c9c58ae09084dfa79a16692acd3823d09bba))
* increase version of maven-war-plugin ([6bd0a0a](https://gitlab.gwdg.de/dariah-de/dariah-de-digilib-services/commit/6bd0a0ad9294d3639b2941e2d377be51f332df7c))


### Features

* add support for CXF4 and Java17 ([1cded48](https://gitlab.gwdg.de/dariah-de/dariah-de-digilib-services/commit/1cded484bfad4c81fc1c0284b51908b44f38e0f6))

## [2.7.2](https://gitlab.gwdg.de/dariah-de/dariah-de-digilib-services/compare/v2.7.1...v2.7.2) (2023-08-25)


### Bug Fixes

* add one more URI "/" replacement ([ec8f3a9](https://gitlab.gwdg.de/dariah-de/dariah-de-digilib-services/commit/ec8f3a95bb4a8fcb3f3b8349626831e6bba448d6))

## [2.7.1](https://gitlab.gwdg.de/dariah-de/dariah-de-digilib-services/compare/v2.7.0...v2.7.1) (2023-08-17)


### Bug Fixes

* add info logging ([c50767d](https://gitlab.gwdg.de/dariah-de/dariah-de-digilib-services/commit/c50767d3a28279908cf28e8f135c3173f24c8c81))
* add logging ([3d947f6](https://gitlab.gwdg.de/dariah-de/dariah-de-digilib-services/commit/3d947f6f6c5f1ddf4a6d452f4dd70efdd466068a))
* add more logging ([18b34ae](https://gitlab.gwdg.de/dariah-de/dariah-de-digilib-services/commit/18b34ae73d59dc630bbb8a31a4426fe945b71e35))
* adding loghmpf ([dbedbbd](https://gitlab.gwdg.de/dariah-de/dariah-de-digilib-services/commit/dbedbbd1485c07b9cd38da268a070fe86574f017))
* fix reading last modified date from admmd ([6821b53](https://gitlab.gwdg.de/dariah-de/dariah-de-digilib-services/commit/6821b53b6965cc98ba8834efdf8bbf944840d33a))
* increase some versions in main pom file ([583b83d](https://gitlab.gwdg.de/dariah-de/dariah-de-digilib-services/commit/583b83d7e2f4995fcfe83d5cecf3faa02ac8bff9))
* remove unused logging ([44ef00c](https://gitlab.gwdg.de/dariah-de/dariah-de-digilib-services/commit/44ef00cd918f53d71af804a2d62a20b47927f579))
* replace %2F with / in URI for dhcrud call ([1b24a7b](https://gitlab.gwdg.de/dariah-de/dariah-de-digilib-services/commit/1b24a7b7ad1cd22b697f1f3efa0159acf8f19f94))
* using admmd instead of dmd now for date usage ([8c405c1](https://gitlab.gwdg.de/dariah-de/dariah-de-digilib-services/commit/8c405c10a26f1ebd2b3d709abd4094206517a2ce))
* verschtehichallesnischt ([e3730c6](https://gitlab.gwdg.de/dariah-de/dariah-de-digilib-services/commit/e3730c69846fbb86582a9fa30c7018d5e4e69a42))

# [2.7.0](https://gitlab.gwdg.de/dariah-de/dariah-de-digilib-services/compare/v2.6.1...v2.7.0) (2023-02-28)


### Features

* drop aptly deployment ([fced788](https://gitlab.gwdg.de/dariah-de/dariah-de-digilib-services/commit/fced788f04fe4e7e593cef16d14f187e194ca2ed))
* tgsearch and tgcrud locations independently configurable, but with usefull defaults ([09a790a](https://gitlab.gwdg.de/dariah-de/dariah-de-digilib-services/commit/09a790a329a321453df23884520aae028493fbbc))

## [2.6.1](https://gitlab.gwdg.de/dariah-de/dariah-de-digilib-services/compare/v2.6.0...v2.6.1) (2022-11-08)


### Bug Fixes

* fix release workflow (no deb on tag) ([1dcc365](https://gitlab.gwdg.de/dariah-de/dariah-de-digilib-services/commit/1dcc365ffdf0deaa4bdcf132b9e42e067bdc7d9b))

# [2.6.0](https://gitlab.gwdg.de/dariah-de/dariah-de-digilib-services/compare/v2.5.7...v2.6.0) (2022-11-08)


### Features

* add new gitlab ci workflow ([dd39aa2](https://gitlab.gwdg.de/dariah-de/dariah-de-digilib-services/commit/dd39aa2d6c4afbecb4f423542920f5fbf9d376d6))

# 2.5.7

* Add new gitlab CI workflow

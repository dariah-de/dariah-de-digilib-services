.. dh-digilib documentation master file, created by
   sphinx-quickstart on Thu May 21 12:41:35 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


DH-digilib Service
==================

DH-digilib is a version of digilib (see digilib_), which works with DARIAH-DE Repository Handles and is integrated within the DARIAH-DE Repository.


API Documentation
------------------

Digilib supports the digilib_scaler_api_ and the iiif_image_api_ for retrieving DARIAH-DE Repository image objects.

Digilib scaler API example **request**::

    https://repository.de.dariah.eu/1.0/digilib/rest/digilib/hdl:21.11113%2F0000-000B-D268-3?dh=500&dw=500

**Response**: 500px preview image

.. image:: pics/hdl_21_11113_0000-000B-D268-3.jpg
    :target: https://repository.de.dariah.eu/1.0/digilib/rest/digilib/hdl:21.11113%2F0000-000B-D268-3?dh=500&dw=500

IIIF Image API example **request**::

    https://repository.de.dariah.eu/1.0/digilib/rest/IIIF/hdl:21.11113%2F0000-000B-D268-3/full/,500/0/native.jpg

Response: 500px preview image

.. image:: pics/hdl_21_11113_0000-000B-D268-3.jpg
    :target: https://repository.de.dariah.eu/1.0/digilib/rest/IIIF/hdl:21.11113%2F0000-000B-D268-3/full/,500/0/native.jpg

For a complete reference see IIIF Image API.


Sources
-------

See dh-digilib_sources_


Bugtracking
-----------

See dh-digilib_bugtracking_


License
-------

See LICENCE_

.. _LICENCE: https://gitlab.gwdg.de/dariah-de/dariah-de-digilib-services/-/blob/main/LICENSE.txt
.. _dh-digilib_sources: https://gitlab.gwdg.de/dariah-de/dariah-de-digilib-services
.. _dh-digilib_bugtracking: https://gitlab.gwdg.de/dariah-de/dariah-de-digilib-services/-/issues
.. _digilib: http://digilib.sourceforge.net/
.. _digilib_scaler_api: http://digilib.sourceforge.net/scaler-api.html
.. _iiif_image_api: http://iiif.io/api/image/2.1/

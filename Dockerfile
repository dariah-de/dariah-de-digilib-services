###
# build
###
FROM maven:3.8.7-eclipse-temurin-17 as builder

ARG JOLOKIA_VERSION="1.3.7" # jolokia as of 1.5.0 requires auth for proxy operation

# download and explode jolokia
RUN curl -XGET "https://repo1.maven.org/maven2/org/jolokia/jolokia-war/${JOLOKIA_VERSION}/jolokia-war-${JOLOKIA_VERSION}.war" --output /jolokia.war
RUN mkdir /jolokia && cd /jolokia && jar -xf ../jolokia.war

COPY . /build
WORKDIR /build

# build and assemble app
RUN --mount=type=cache,target=/root/.m2 mvn clean package

###
# assemble image
###
FROM tomcat:10.1-jre17

ENV JAVA_OPTS="-Xmx768m"
ENV CATALINA_OPTS="-Dcom.sun.management.jmxremote.port=9999 -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false"

COPY Dockerfile /

RUN addgroup --system --gid 1009 tomcat-digilib
RUN adduser --system --uid 1009 --gid 1009 tomcat-digilib

COPY --from=builder /jolokia /usr/local/tomcat/webapps/jolokia
COPY --from=builder /build/service/target/digilibservice /usr/local/tomcat/webapps/digilibservice

RUN chown -R tomcat-digilib:tomcat-digilib /usr/local/tomcat/webapps/

USER tomcat-digilib
WORKDIR /usr/local/tomcat/
EXPOSE 8080



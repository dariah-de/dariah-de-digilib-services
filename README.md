# DARIAH-DE Digilib Services

![dariah-de-digilib-services-logo](./docs_dhrep/pics/DARIAH-DE_Digilib-Services.png)

This service deliveres images in Digilib specification.


## Releasing a new version

For releasing a new version of TG-crud or DH-crud, please have a look at the [DARIAH-DE Release Management Page](https://wiki.de.dariah.eu/display/DARIAH3/DARIAH-DE+Release+Management#DARIAHDEReleaseManagement-Gitlabflow/Gitflow(develop,main,featurebranchesundtags)) or see the [Gitlab CI file](.gitlab-ci.yml).

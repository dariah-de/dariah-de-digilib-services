package info.textgrid.services.digilib.service.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import org.apache.cxf.helpers.IOUtils;
import org.junit.Test;
import org.xml.sax.SAXException;
import junit.framework.TestCase;

/**
 *
 */
public class TestBaseScaler extends TestCase {

  private static final String FITS_OUTPUT_LOCATION = "src/test/resources/fits_output.xml";
  private static final String FITS_OUTPUT_LOCATION_WITHOUT_HEIGHT =
      "src/test/resources/fits_output_without_height.xml";
  private static final String PATH_IMAGE_WIDTH = "fits/metadata/image/imageWidth";
  private static final String PATH_IMAGE_HEIGHT = "fits/metadata/image/imageHeight";
  private static final String PATH_Y_SAMPLING_FREQUENCY = "fits/metadata/image/xSamplingFrequency";
  private static final String PATH_X_SAMPLING_FREQUENCY = "fits/metadata/image/ySamplingFrequency";

  /**
   *
   */
  public void testGetCacheFile() {

    String tgUri = "textgrid:1234.0";
    String expTgPath = "12/1234.0";
    String resTgPath = BaseScaler.getPathFromUri(tgUri, true);
    if (!expTgPath.equals(resTgPath)) {
      System.out.println(tgUri + " = " + resTgPath);
      assertTrue(false);
    }

    String dhTestHdl = "hdl:21.T11991/0000-0008-B832-0";
    String expDhTestPath = "21.T11991-0000-0008/21.T11991-0000-0008-B832-0";
    String resDhTestPath = BaseScaler.getPathFromUri(dhTestHdl, true);
    if (!expDhTestPath.equals(resDhTestPath)) {
      System.out.println(dhTestHdl + " = " + resDhTestPath);
      assertTrue(false);
    }

    String dhProdHdl = "hdl:21.11113/0000-000B-CAC4-4";
    String expDhProdPath = "21.11113-0000-000B/21.11113-0000-000B-CAC4-4";
    String resDhProdPath = BaseScaler.getPathFromUri(dhProdHdl, true);
    if (!expDhProdPath.equals(resDhProdPath)) {
      System.out.println(dhProdHdl + " = " + resDhProdPath);
      assertTrue(false);
    }
  }

  /**
   *
   */
  public void testGetCacheFileEscaped() {

    String dhTestHdl = "hdl:21.T11991%2F0000-0008-B832-0";
    String expDhTestPath = "21.T11991-0000-0008/21.T11991-0000-0008-B832-0";
    String resDhTestPath = BaseScaler.getPathFromUri(dhTestHdl, true);
    if (!expDhTestPath.equals(resDhTestPath)) {
      System.out.println(resDhTestPath + " != " + expDhTestPath);
      assertTrue(false);
    }

    String dhProdHdl = "hdl:21.11113%2F0000-000B-CAC4-4";
    String expDhProdPath = "21.11113-0000-000B/21.11113-0000-000B-CAC4-4";
    String resDhProdPath = BaseScaler.getPathFromUri(dhProdHdl, true);
    if (!expDhProdPath.equals(resDhProdPath)) {
      System.out.println(resDhProdPath + " != " + expDhProdPath);
      assertTrue(false);
    }
  }

  /**
   * @throws XPathExpressionException
   * @throws FileNotFoundException
   * @throws ParserConfigurationException
   * @throws SAXException
   * @throws IOException
   */
  @Test
  public void testGetEXIFMetadata() throws XPathExpressionException, FileNotFoundException,
      ParserConfigurationException, SAXException, IOException {

    int expectedWidth = 5676;
    int expectedHeight = 4446;
    int expectedXSFreq = 600;
    int expectedYSFreq = 600;

    System.out.println("Testing extract EXIF metadata from FITS output: " + FITS_OUTPUT_LOCATION);

    int imageWidth =
        Integer.parseInt(BaseScaler.getXPathExpressionValue(IOUtils.readStringFromStream(
            new FileInputStream(new File(FITS_OUTPUT_LOCATION))), PATH_IMAGE_WIDTH));
    if (imageWidth != expectedWidth) {
      System.out.println(imageWidth + " != " + expectedWidth);
      assertTrue(false);
    }
    int imageHeight =
        Integer.parseInt(BaseScaler.getXPathExpressionValue(IOUtils.readStringFromStream(
            new FileInputStream(new File(FITS_OUTPUT_LOCATION))), PATH_IMAGE_HEIGHT));
    if (imageHeight != expectedHeight) {
      System.out.println(imageHeight + " != " + expectedHeight);
      assertTrue(false);
    }
    int xSamplingFreq =
        Integer.parseInt(BaseScaler.getXPathExpressionValue(
            IOUtils.readStringFromStream(new FileInputStream(new File(FITS_OUTPUT_LOCATION))),
            PATH_X_SAMPLING_FREQUENCY));
    if (xSamplingFreq != expectedXSFreq) {
      System.out.println(xSamplingFreq + " != " + expectedXSFreq);
      assertTrue(false);
    }
    int ySamplingFreq = Integer.parseInt(BaseScaler.getXPathExpressionValue(
        IOUtils.readStringFromStream(new FileInputStream(new File(FITS_OUTPUT_LOCATION))),
        PATH_Y_SAMPLING_FREQUENCY));
    if (ySamplingFreq != expectedYSFreq) {
      System.out.println(ySamplingFreq + " != " + expectedYSFreq);
      assertTrue(false);
    }
  }

  /**
   * @throws XPathExpressionException
   * @throws FileNotFoundException
   * @throws ParserConfigurationException
   * @throws SAXException
   * @throws IOException
   */
  @Test
  public void testGetEXIFMetadataWithoutHeight() throws XPathExpressionException,
      FileNotFoundException, ParserConfigurationException, SAXException, IOException {

    int expectedWidth = 2785;
    int expectedHeight = 0;
    int expectedXSFreq = 0;
    int expectedYSFreq = 0;

    System.out.println(
        "Testing extract EXIF metadata from FITS output: " + FITS_OUTPUT_LOCATION_WITHOUT_HEIGHT);

    String width = BaseScaler.getXPathExpressionValue(IOUtils.readStringFromStream(
        new FileInputStream(new File(FITS_OUTPUT_LOCATION_WITHOUT_HEIGHT))), PATH_IMAGE_WIDTH);
    if (width != null) {
      int imageWidth = Integer.parseInt(width);
      if (imageWidth != expectedWidth) {
        System.out.println(imageWidth + " != " + expectedWidth);
        assertTrue(false);
      }
    }

    String height = BaseScaler.getXPathExpressionValue(IOUtils.readStringFromStream(
        new FileInputStream(new File(FITS_OUTPUT_LOCATION_WITHOUT_HEIGHT))),
        PATH_IMAGE_HEIGHT);
    if (height != null) {
      int imageHeight = Integer.parseInt(height);
      if (imageHeight != expectedHeight) {
        System.out.println(imageHeight + " != " + expectedHeight);
        assertTrue(false);
      }
    }

    String xsf = BaseScaler.getXPathExpressionValue(
        IOUtils.readStringFromStream(
            new FileInputStream(new File(FITS_OUTPUT_LOCATION_WITHOUT_HEIGHT))),
        PATH_X_SAMPLING_FREQUENCY);
    if (xsf != null) {
      int xSamplingFreq = Integer.parseInt(xsf);
      if (xSamplingFreq != expectedXSFreq) {
        System.out.println(xSamplingFreq + " != " + expectedXSFreq);
        assertTrue(false);
      }
    }

    String ysf = BaseScaler.getXPathExpressionValue(
        IOUtils.readStringFromStream(
            new FileInputStream(new File(FITS_OUTPUT_LOCATION_WITHOUT_HEIGHT))),
        PATH_Y_SAMPLING_FREQUENCY);
    if (ysf != null) {
      int ySamplingFreq = Integer.parseInt(ysf);
      if (ySamplingFreq != expectedYSFreq) {
        System.out.println(ySamplingFreq + " != " + expectedYSFreq);
        assertTrue(false);
      }
    }
  }

}

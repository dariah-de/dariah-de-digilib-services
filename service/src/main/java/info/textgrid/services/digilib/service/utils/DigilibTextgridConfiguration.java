package info.textgrid.services.digilib.service.utils;

/*
 * #%L digilib-service %% Copyright (C) 2011 - 2014 MPIWG Berlin %% This program is free software:
 * you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>. #L% Author: Robert Casties
 * (casties@mpiwg-berlin.mpg.de)
 */

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;

import digilib.conf.DigilibConfiguration;
import digilib.io.DocuDirCache;
import digilib.io.DocuDirectory;
import digilib.io.DocuDirectoryFactory;
import digilib.io.DocuDirent;
import digilib.io.DocuDirentFactory;
import digilib.io.FileOps;
import digilib.io.FileOps.FileClass;
import digilib.util.Parameter;

/**
 * @author casties
 */
public class DigilibTextgridConfiguration extends DigilibConfiguration
    implements ServletContextListener {

  public static final String SERVLET_CONFIG_KEY = "digilib.service.configuration";

  public static final String DIR_CACHE_KEY = "servlet.dir.cache";

  public static final String DIR_CLASS_KEY = "docudirectory-class";

  public static final String DIR_IMAGE_CLASS_KEY = "docudirectory-image-class";

  /**
   * Constructs DigilibTextgridConfiguration and defines all parameters and their default values.
   */
  public DigilibTextgridConfiguration() {
    // take all parameters from DigilibConfiguration
    super();
    /*
     * Definition of parameters and default values. System parameters that are not read from a
     * config file have a type 's'.
     */
    // DocuDirCache instance
    newParameter(DIR_CACHE_KEY, null, null, 's');
    newParameter("service.docudirectory.class", null, null, 's');
    /*
     * parameters that can be read from config file have a type 'f'
     */
    // base directories in order of preference (prescaled versions last)
    String[] bd = {"sample-images"};
    newParameter("basedir-list", bd, null, 'f');
    // DocuDirectory implementation
    newParameter(DIR_CLASS_KEY, "digilib.io.BaseDirDocuDirectory", null, 'f');
    // DocuDirectory Image member implementation
    newParameter(DIR_IMAGE_CLASS_KEY, "info.textgrid.services.digilib.service.utils.RepImageFileSet", null, 'f');

  }

  /*
   * (non-Javadoc)
   * 
   * @see digilib.conf.DigilibConfiguration#configure()
   */
  @SuppressWarnings("unchecked")
  @Override
  public void configure() {
    DigilibTextgridConfiguration config = this;
    super.configure();
    try {
      // initialise DocuDirent factory
      Class<DocuDirent> docuDirentClass =
          (Class<DocuDirent>) Class.forName(config.getAsString(DIR_IMAGE_CLASS_KEY));
      config.setValue("service.docudirectory.image.class", docuDirentClass);
      DocuDirentFactory.setDocuDirentClass(FileClass.IMAGE, docuDirentClass);
      // initialise DocuDirectoryFactory
      Class<DocuDirectory> docuDirectoryClass =
          (Class<DocuDirectory>) Class.forName(config.getAsString(DIR_CLASS_KEY));
      config.setValue("service.docudirectory.class", docuDirectoryClass);
      DocuDirectoryFactory.setDocuDirectoryClass(docuDirectoryClass);
      DocuDirectoryFactory.setDigilibConfig(this);
    } catch (ClassNotFoundException e) {
      logger.error("Error setting up DocuDirectory!", e);
    }
    // directory cache (using DocuDirectoryFactory)
    DocuDirCache dirCache = new DocuDirCache(FileClass.IMAGE, this);
    config.setValue(DIR_CACHE_KEY, dirCache);
  }

  /**
   * Initialisation on first run.
   */
  @Override
  public void contextInitialized(ServletContextEvent cte) {
    ServletContext context = cte.getServletContext();
    context.log("***** digilib TextGrid service Configuration (" + getVersion() + ") *****");
    // see if there is a Configuration instance
    DigilibTextgridConfiguration dlConfig = getCurrentConfig(context);
    if (dlConfig == null) {
      try {
        // initialise this instance
        readConfig();
        configure();
        setCurrentConfig(context);
      } catch (Exception e) {
        logger.error("Error reading digilib service configuration:", e);
      }
    } else {
      // say hello in the log file
      logger.warn("DigilibTextgridConfiguration already configured!");
    }
  }

  /**
   * Clean up local resources
   * 
   */
  @Override
  public void contextDestroyed(ServletContextEvent arg0) {
    logger.info("DigilibServletConfiguration shutting down.");
    // anything to do?
  }

  /**
   * Sets the current DigilibConfiguration in the context.
   * 
   * @param context
   */
  public void setCurrentConfig(ServletContext context) {
    context.setAttribute(DigilibTextgridConfiguration.SERVLET_CONFIG_KEY, this);
  }

  /**
   * Returns the current DigilibConfiguration from the context.
   * 
   * @param context
   * @return
   */
  public static DigilibTextgridConfiguration getCurrentConfig(ServletContext context) {
    DigilibTextgridConfiguration config = (DigilibTextgridConfiguration) context
        .getAttribute(DigilibTextgridConfiguration.SERVLET_CONFIG_KEY);
    return config;
  }

  /*
   * (non-Javadoc)
   * 
   * @see digilib.conf.DigilibConfiguration#setSpecialValueFromString(digilib.util.Parameter,
   * java.lang.String)
   */
  @Override
  protected boolean setSpecialValueFromString(Parameter param, String value) {
    String name = param.getName();
    if (name == "basedir-list") {
      // split list into directories
      String[] dirs = FileOps.pathToArray(value);
      if (dirs != null) {
        param.setValue(dirs);
        return true;
      }
    }
    return false;
  }

}

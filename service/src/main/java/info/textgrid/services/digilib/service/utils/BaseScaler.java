package info.textgrid.services.digilib.service.utils;

/*
 * #%L digilib-service %% Copyright (C) 2011 - 2014 MPIWG Berlin, SUB Göttingen %% This program is
 * free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 * #L% Author: Robert Casties <casties@mpiwg-berlin.mpg.de>, Jorge Urzua
 * <jurzua@mpiwg-berlin.mpg.de>, Ubbo Veentjer <veentjer@sub.uni-goettingen.de>, Stefan E. Funk
 * <funk@sub.uni-goettingen.de>
 */

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;
import jakarta.activation.DataHandler;
import jakarta.servlet.ServletContext;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import jakarta.xml.ws.Holder;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.apache.cxf.helpers.IOUtils;
import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3._1999._02._22_rdf_syntax_ns_.RdfType;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import digilib.conf.DigilibConfiguration;
import digilib.conf.DigilibOption;
import digilib.conf.DigilibRequest;
import digilib.image.DocuImage;
import digilib.image.ImageJobDescription;
import digilib.image.ImageLoaderDocuImage;
import digilib.image.ImageOpException;
import digilib.image.ImageWorker;
import digilib.io.DocuDirCache;
import digilib.io.ImageCacheStream;
import digilib.io.ImageInput;
import digilib.io.ImageSet;
import digilib.io.ImageStream;
import digilib.meta.MetadataMap;
import digilib.meta.SimpleFileMeta;
import digilib.util.ImageSize;
import info.textgrid.clients.SearchClient;
import info.textgrid.namespaces.metadata.core._2010.MetadataContainerType;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import info.textgrid.namespaces.metadata.core._2010.RelationType;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.AuthFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.DHCrudService;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.MetadataParseFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ObjectNotFoundFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ProtocolNotImplementedFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.TGCrudService;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.tgcrudclient.TGCrudClientUtilities;
import info.textgrid.namespaces.middleware.tgsearch.Revisions;

/**
 *
 */
public abstract class BaseScaler {

  protected static final String GLOBAL_CONFIG_FILE =
      "/etc/dhrep/digilib/digilib-service.properties";
  protected static final String TGREP_PREFIX = "textgrid:";
  protected static final String DHREP_PREFIX = "hdl:";

  protected static final Logger logger = LoggerFactory.getLogger(BaseScaler.class);
  public static AtomicInteger runningJobs = new AtomicInteger();

  protected String textgridServerUrl;
  protected TGCrudService tgcrud;
  protected String tgcrudServerUrl;
  protected DHCrudService dhcrud;
  protected String dhcrudServerUrl;
  protected String tgsearchServerBaseUrl;
  protected SearchClient searchClientPublic;
  protected SearchClient searchClientNonPublic;
  protected DigilibConfiguration dlConfig;
  protected DocuDirCache dirCache;
  protected boolean usePrescaleSubdirs = true;
  /** configuration properties for TextGrid-servlet */
  protected Properties props;

  public static final String PATH_IMAGE_WIDTH = "fits/metadata/image/imageWidth";
  public static final String PATH_IMAGE_HEIGHT = "fits/metadata/image/imageHeight";
  public static final String PATH_Y_SAMPLING_FREQUENCY = "fits/metadata/image/xSamplingFrequency";
  public static final String PATH_X_SAMPLING_FREQUENCY = "fits/metadata/image/ySamplingFrequency";

  /**
   * @return
   */
  public String getVersion() {
    return "1.9.0 (digilib " + DigilibConfiguration.getClassVersion() + ")";
  }

  /**
   *
   */
  public BaseScaler() {
    logger.info("digilibservice-Scaler V" + getVersion() + " starting...");
    /*
     * configure servlet
     */
    try {
      this.props = new Properties();
      InputStream s = null;
      File globalConfFile = new File(GLOBAL_CONFIG_FILE);
      if (globalConfFile.canRead()) {
        s = new FileInputStream(globalConfFile);
      } else {
        logger.warn("Unable to read config file " + globalConfFile.getCanonicalPath()
            + ". trying to use classpath.");
        s = Thread.currentThread().getContextClassLoader()
            .getResourceAsStream("digilib-service.properties");
      }
      if (s != null) {
        this.props.load(s);
        s.close();
        this.textgridServerUrl =
            this.props.getProperty("digilib.service.textgrid.server", "https://textgridlab.org");
        this.tgcrudServerUrl = this.props.getProperty("digilib.service.tgcrudurl",
            this.textgridServerUrl + "/1.0/tgcrud/TGCrudService?wsdl");
        this.tgsearchServerBaseUrl =
            this.props.getProperty("digilib.service.tgsearchbaseurl",
                this.textgridServerUrl + "/1.0");
        this.dhcrudServerUrl =
            this.props.getProperty("digilib.service.dhcrudurl", this.dhcrudServerUrl);
        this.usePrescaleSubdirs =
            Boolean.parseBoolean(this.props.getProperty("digilib.prescale.subdirs", "true"));
      } else {
        logger.warn("Properties file digilib-service.properties not found! Using defaults.");
      }
    } catch (Exception e) {
      logger.warn("Unable to load properties for servlet:", e);
    }

    /*
     * configure tgcrud
     */
    try {
      if (this.tgcrudServerUrl != null) {
        logger.debug("TG-crud URL: " + this.tgcrudServerUrl);
        createTgCrudService();
      } else {
        throw new IOException("No TG-crud URL: "
            + (this.tgcrudServerUrl == null ? "null" : this.tgcrudServerUrl) + "!");
      }
    } catch (IOException e) {
      logger.error(e.getMessage(), e);
    }

    /*
     * configure dhcrud
     */
    try {
      if (this.dhcrudServerUrl != null) {
        logger.debug("DH-crud URL: " + this.dhcrudServerUrl);
        createDhCrudService();
      } else {
        throw new IOException(
            "No DH-crud URL: " + (this.dhcrudServerUrl == null ? "null" : this.dhcrudServerUrl)
                + "!");
      }
    } catch (IOException e) {
      logger.error(e.getMessage(), e);
    }

    this.searchClientPublic = new SearchClient(this.tgsearchServerBaseUrl + "/tgsearch-public")
        .enableGzipCompression();
    this.searchClientNonPublic = new SearchClient(this.tgsearchServerBaseUrl + "/tgsearch")
        .enableGzipCompression();
  }

  /**
   * Reads metadata from TGCrud or DHCrud and populates the response object.
   *
   * Sets error in response if uri doesn't exist or session is not allowed to read.
   *
   * @param sessionId
   * @param uri
   * @param logParameter
   * @param response
   * @return
   */
  protected ScalerResponse readMeta(final String sessionId, final String uri,
      final String logParameter, ScalerResponse response) {
    if (response == null) {
      response = new ScalerResponse();
    }
    /*
     * look for cached ImageSet with metadata
     */
    String path = getPathFromUri(uri, this.usePrescaleSubdirs); // TODO should we
    // tgmeta.getGeneric().getGenerated().getTextgridUri().getValue()
    RepImageFileSet imgSet = (RepImageFileSet) this.dirCache.getFile(path, 0);
    if (imgSet != null) {
      response.imageSet = imgSet;
      String cachedSid = imgSet.getSid();
      // check if cached SID matches (new cache has null SID)
      if (sessionId.equals(cachedSid)) {
        /*
         * read metadata from cached ImageSet
         */
        ImageInput img = imgSet.get(0);
        if (img.isChecked()) {
          logger.debug("Reading metadata from cache.");
          ImageSize imgSize = img.getSize();
          response.originalHeight = imgSize.getHeight();
          response.originalWidth = imgSize.getWidth();
          // set mtime in response
          response.mtime = imgSet.getRepMtime();
          return response;
        }
      }
    }
    /*
     * read metadata from Rep
     */
    if (uri.startsWith(TGREP_PREFIX)) {
      response = readTGRepMeta(sessionId, uri, logParameter, response);
    } else if (uri.startsWith(DHREP_PREFIX)) {
      response = readDHRepMeta(uri, response);
    }

    if (imgSet != null && !response.hasError()) {
      /*
       * save metadata to cacheable ImageSet
       */
      imgSet.setSid(sessionId);
      imgSet.setRepMtime(response.mtime);
      ImageInput img = imgSet.get(0);
      img.setMimetype(response.mimeType);
      if (response.originalHeight != null) {
        ImageSize size = new ImageSize(response.originalWidth, response.originalHeight);
        img.setSize(size);
      }
      if (response.originalDpi != null) {
        if (imgSet.getMeta() == null) {
          imgSet.setMeta(new SimpleFileMeta(new MetadataMap()));
        }
        imgSet.getMeta().getFileMeta().put("original-dpi", response.originalDpi.toString());
      }
    }

    return response;
  }

  /**
   * @param sessionId
   * @param uri
   * @param logParameter
   * @param response
   * @return
   */
  private ScalerResponse readTGRepMeta(final String sessionId, final String uri,
      final String logParameter, ScalerResponse response) {
    if (this.tgcrud == null) {
      // Create TG-crud service stub.
      createTgCrudService();
    }
    try {
      /*
       * TGCrud #readMetadata
       */
      logger.debug("TGCRUD-READMETA: uri=" + uri);
      MetadataContainerType mc = this.tgcrud.readMetadata(sessionId, logParameter, uri);
      ObjectType tgmeta = mc.getObject();
      logger.debug("Meta title=" + tgmeta.getGeneric().getProvided().getTitle().get(0));
      // get mime-type
      String mt = tgmeta.getGeneric().getProvided().getFormat();
      response.mimeType = mt;
      logger.debug("Meta mime-type=" + mt);
      // get modification-date
      long md = tgmeta.getGeneric().getGenerated().getLastModified().toGregorianCalendar()
          .getTimeInMillis();
      // round to full seconds (else If-Modified-Since check won't work)
      response.mtime = (md / 1000 * 1000);
      logger.debug("Meta last-modified=" + md);
      // get full TextGrid URI
      String tguri = tgmeta.getGeneric().getGenerated().getTextgridUri().getValue();
      logger.debug("Meta tg-uri=" + tguri);
      response.cacheLocation = tguri;
      // try to get image size
      RelationType rel = tgmeta.getRelations();
      if (rel != null) {
        RdfType rdf = rel.getRDF();
        if (rdf != null) {
          List<Object> rdfs = rdf.getAny();
          for (Object o : rdfs) {
            Element e = (Element) o;
            // try to read EXIF size from RDF
            response.originalHeight = getExifInfo(e, "height");
            response.originalWidth = getExifInfo(e, "width");
            Integer xres = getExifInfo(e, "xResolution");
            Integer yres = getExifInfo(e, "yResolution");
            response.originalDpi = (xres != null) ? xres : yres;
            logger.debug("meta exif height=" + response.originalHeight + " width="
                + response.originalWidth + " dpi=" + response.originalDpi);
          }
        }
      }

    } catch (ObjectNotFoundFault e) {
      logger.error("Object not found", e);
      response.errorCode = 404;
      response.errorMsg = "Object not found!";
    } catch (MetadataParseFault e) {
      logger.error("Metadata error", e);
      response.errorCode = 500;
      response.errorMsg = "Metadata error!";
    } catch (IoFault e) {
      logger.error("IO fault", e);
      response.errorCode = 500;
      response.errorMsg = "IO fault!";
    } catch (AuthFault e) {
      logger.error("Auth fault", e);
      response.errorCode = 401;
      response.errorMsg = "Auth fault!";
    }

    return response;
  }

  /**
   * @param theUri
   * @param response
   * @return
   */
  private ScalerResponse readDHRepMeta(final String theUri, ScalerResponse response) {
    if (this.dhcrud == null) {
      // Create DH-crud service stub.
      createDhCrudService();
    }
    /*
     * DHCrud #readMetadata
     */
    logger.debug("DHCRUD-READMETA: uri=" + theUri);
    // Seems we have to replace a "%2F" here with "/"...?
    URI uri = URI.create(replaceSlashEscapeChar(theUri));
    Response admmd = this.dhcrud.readAdmMD(uri, "");
    try {
      checkResponse(admmd);
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    // get mime-type
    String mt = admmd.getMediaType().toString();
    response.mimeType = mt;
    logger.debug("Meta mime-type=" + mt);
    // get modification-date
    long md = admmd.getLastModified().getTime();
    // round to full seconds (else If-Modified-Since check won't work)
    response.mtime = (md / 1000 * 1000);
    logger.debug("Meta last-modified=" + md);
    // get full URI
    logger.debug("Meta dh-uri=" + uri);
    response.cacheLocation = uri.toString();

    // try to get image size
    logger.debug("DHCRUD-READTECH: uri=" + uri);
    Response techmd = this.dhcrud.readTechMD(uri, "");
    try {
      checkResponse(techmd);
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    String fitsXML = "";
    try {
      fitsXML = IOUtils.readStringFromStream((InputStream) techmd.getEntity());
      String ow = getXPathExpressionValue(fitsXML, PATH_IMAGE_WIDTH);
      if (ow != null) {
        response.originalWidth = Integer.valueOf(ow);
      }
      String oh = getXPathExpressionValue(fitsXML, PATH_IMAGE_HEIGHT);
      if (oh != null) {
        response.originalHeight = Integer.valueOf(oh);
      }
      String xr = getXPathExpressionValue(fitsXML, PATH_X_SAMPLING_FREQUENCY);
      if (xr != null) {
        Integer xres = Integer.valueOf(xr);
        response.originalDpi = xres;
      }
      String yr = getXPathExpressionValue(fitsXML, PATH_Y_SAMPLING_FREQUENCY);
      if (yr != null) {
        Integer yres = Integer.valueOf(yr);
        response.originalDpi = yres;
      }
    } catch (IOException e) {
      // TODO Which exceptions can be expected here?
      e.printStackTrace();
    }

    logger.debug("meta exif height=" + response.originalHeight + " width=" + response.originalWidth
        + " dpi=" + response.originalDpi);

    return response;
  }

  /**
   * Reads data from TGCrud, scales the image and returns data in the response object.
   *
   * Uses parameters from dlReq for scaling. Sets error in response if anything goes wrong.
   *
   * Needs BaseScaler.dlConfig to be set.
   *
   * @param sessionId
   * @param uri
   * @param logParameter
   * @param dlReq
   * @param response
   * @return
   */
  protected ScalerResponse scale(final String sessionId, final String uri,
      final String logParameter, DigilibRequest dlReq, ScalerResponse response) {
    if (response == null) {
      // read metadata first
      response = readMeta(sessionId, uri, logParameter, null);
    }
    if (uri.startsWith(TGREP_PREFIX)) {
      response = scaleTGRep(sessionId, uri, logParameter, dlReq, response);
    } else if (uri.startsWith(DHREP_PREFIX)) {
      response = scaleDHRep(uri, logParameter, dlReq, response);
    }

    return response;
  }

  /**
   * @param sessionId
   * @param uri
   * @param logParameter
   * @param dlReq
   * @param response
   * @return
   */
  protected ScalerResponse scaleTGRep(final String sessionId, final String uri,
      final String logParameter, DigilibRequest dlReq, ScalerResponse response) {
    if (this.tgcrud == null) {
      // Create TG-crud service stub.
      createTgCrudService();
    }
    if (this.dlConfig == null) {
      // try dlConfig from dlReq
      this.dlConfig = dlReq.getDigilibConfig();
      if (this.dlConfig == null) {
        logger.error("No digilib config!");
      }
    }

    // Create holders.
    Holder<MetadataContainerType> metadataHolder = new Holder<MetadataContainerType>();
    Holder<DataHandler> dataHolder = new Holder<DataHandler>();

    logger.debug("Running Scaler jobs: " + BaseScaler.runningJobs.incrementAndGet());
    ImageStream repImg = null;
    try {
      long startTime = System.currentTimeMillis();

      /*
       * set up input for digilib
       */
      String imgPath = getPathFromUri(uri, this.usePrescaleSubdirs);
      // get ImageSet from cache
      ImageSet imgSet = (RepImageFileSet) this.dirCache.getFile(imgPath, 0);
      if (imgSet != null) {
        // re-use ImageStream from cached ImageSet
        repImg = (ImageStream) imgSet.get(0);
      } else {
        // construct new ImageSet
        imgSet = new ImageSet();
        response.imageSet = imgSet;
        // start with dummy tgCrud image
        repImg = new ImageCacheStream(null, response.mimeType);
        // first image is TGCrud original
        imgSet.add(repImg);
        if (response.originalHeight != null && response.originalWidth != null) {
          // set size from metadata
          repImg.setSize(new ImageSize(response.originalWidth, response.originalHeight));
        } else {
          // use maximum size so we don't have to call TGCrud yet
          repImg.setSize(new ImageSize(Integer.MAX_VALUE, Integer.MAX_VALUE));
        }
      }

      // set up digilib job
      ImageJobDescription job =
          ImageJobDescription.getInstanceWithImageSet(dlReq, imgSet, this.dlConfig);

      /*
       * let digilib choose an input
       */
      ImageInput ii = job.getInput();
      if (ii == repImg) {
        /*
         * read from TGCrud
         */
        setupTGCrudInput(sessionId, uri, logParameter, response, metadataHolder, dataHolder, job,
            repImg);
        if (job.getImgSize().getWidth() == Integer.MAX_VALUE) {
          // reset scale parameters with real image size
          job.setImgSize(repImg.getSize());
          job.prepareScaleParams();
        }
      } else {
        /*
         * read from cached file
         */
        long fmtime = ii.getFile().lastModified();
        if (response.mtime > fmtime) {
          // cached file older than original
          logger.warn("Scaled image file older than original: " + ii.getFile());
          // read from TGCrud instead
          setupTGCrudInput(sessionId, uri, logParameter, response, metadataHolder, dataHolder, job,
              repImg);
          if (job.getImgSize().getWidth() == Integer.MAX_VALUE) {
            // reset scale parameters with real image size
            job.setImgSize(repImg.getSize());
            job.prepareScaleParams();
          }
        } else if (response.canSendFile) {
          // check if we can just send the file
          if (!job.isTransformRequired()) {
            /*
             * send the file
             */
            response.imageFile = ii.getFile();
            logger.debug("Sending file as-is (" + (System.currentTimeMillis() - startTime) + "ms): "
                + ii.getFile());
            response.mimeType = ii.getMimetype();
            return response;
          }
        }
      }

      if (response.hasError()) {
        // error while setting up the input
        return response;
      }

      // return info without scaling
      if (dlReq.hasOption(DigilibOption.info) && response.originalWidth != null
          && response.originalHeight != null) {
        logger.debug("Returning info only (" + (System.currentTimeMillis() - startTime) + "ms)");
        return response;
      }

      ByteArrayOutputStream ostream = new ByteArrayOutputStream();
      String outputFormat = job.getOutputMimeType();

      if (job.isTransformRequired()) {
        /*
         * process image
         */
        ImageWorker digilib = new ImageWorker(this.dlConfig, job);
        logger.debug("Scaling with " + digilib);
        DocuImage img = digilib.call();
        logger.debug(Long.toString(System.currentTimeMillis() - startTime) + " ms");

        /*
         * save result
         */
        // write image to buffer
        img.writeImage(outputFormat, ostream);
        response.width = img.getWidth();
        response.height = img.getHeight();
      } else {
        // image from tgcrud seems to be like requested, no processing required
        logger.debug(
            "Sending file from tgcrud as-is (" + (System.currentTimeMillis() - startTime) + "ms)");
        IOUtils.copy(repImg.getInputStream(), ostream);
        ImageSize osize = repImg.getSize();
        response.width = osize.getWidth();
        response.height = osize.getHeight();
      }

      logger.debug("Written in " + (System.currentTimeMillis() - startTime) + " ms");
      // set buffer in response
      response.imageData = ostream.toByteArray();
      response.mimeType = outputFormat;

      logger.debug("Done.");

    } catch (ObjectNotFoundFault e) {
      logger.error("Object not found", e);
      response.errorCode = 404;
      response.errorMsg = "Object not found!";
    } catch (MetadataParseFault e) {
      logger.error("Metadata error", e);
      response.errorCode = 500;
      response.errorMsg = "Metadata error!";
    } catch (IoFault e) {
      logger.error("IO fault", e);
      response.errorCode = 500;
      response.errorMsg = "IO fault!";
    } catch (ProtocolNotImplementedFault e) {
      logger.error("Protocol not implemented", e);
      response.errorCode = 500;
      response.errorMsg = "Protocol not implemented!";
    } catch (AuthFault e) {
      logger.error("Auth fault", e);
      response.errorCode = 401;
      response.errorMsg = "Auth fault!";
    } catch (IOException e) {
      logger.error("IO Exception", e);
      response.errorCode = 500;
      response.errorMsg = "IO Exception!";
    } catch (ImageOpException e) {
      logger.error("ImageOp exception", e);
      response.errorCode = 400;
      response.errorMsg = "Invalid image operation!";
    } finally {
      if (repImg != null) {
        // remove InputStream reference
        repImg.setInputStream(null);
      }
      BaseScaler.runningJobs.decrementAndGet();
    }

    return response;
  }

  /**
   * @param uri
   * @param logParameter
   * @param dlReq
   * @param response
   * @return
   */
  protected ScalerResponse scaleDHRep(final String uri, final String logParameter,
      DigilibRequest dlReq, ScalerResponse response) {
    if (this.dhcrud == null) {
      // Create DH-crud service stub.
      createDhCrudService();
    }
    if (this.dlConfig == null) {
      // try dlConfig from dlReq
      this.dlConfig = dlReq.getDigilibConfig();
      if (this.dlConfig == null) {
        logger.error("No digilib config!");
      }
    }

    logger.debug("Running Scaler jobs: " + BaseScaler.runningJobs.incrementAndGet());
    ImageStream repImg = null;

    try {
      long startTime = System.currentTimeMillis();

      /*
       * set up digilib inputs
       */
      String imgPath = getPathFromUri(uri, this.usePrescaleSubdirs);
      // get ImageSet from cache
      ImageSet imgSet = (RepImageFileSet) this.dirCache.getFile(imgPath, 0);
      if (imgSet != null) {
        // re-use ImageStream from cached ImageSet
        repImg = (ImageStream) imgSet.get(0);
      } else {
        // construct new ImageSet
        imgSet = new ImageSet();
        response.imageSet = imgSet;
        // start with dummy tgCrud image
        repImg = new ImageCacheStream(null, response.mimeType);
        // first image is TGCrud original
        imgSet.add(repImg);
        if (response.originalHeight != null && response.originalWidth != null) {
          // set size from metadata
          repImg.setSize(new ImageSize(response.originalWidth, response.originalHeight));
        } else {
          // use maximum size so we don't have to call TGCrud yet
          repImg.setSize(new ImageSize(Integer.MAX_VALUE, Integer.MAX_VALUE));
        }
      }
      // set up digilib job
      ImageJobDescription job =
          ImageJobDescription.getInstanceWithImageSet(dlReq, imgSet, this.dlConfig);

      /*
       * let digilib choose an input
       */
      ImageInput ii = job.getInput();
      logger.debug("ii:          " + (ii != null ? ii.toString() : "NULL"));
      logger.debug("dhCrudImage: " + (repImg != null ? repImg.toString() : "NULL"));
      if (ii == null) {
        // this shouldn't happen
        throw new IOException("digilib scaler input is empty!");
      } else if (ii == repImg) {
        /*
         * read from DHCrud
         */
        setupDHCrudInput(uri, logParameter, response, job, repImg);
        if (job.getImgSize().getWidth() == Integer.MAX_VALUE) {
          // reset scale parameters with real image size
          job.setImgSize(repImg.getSize());
          job.prepareScaleParams();
        }
      } else {
        /*
         * read from cached file
         */
        long fmtime = ii.getFile().lastModified();
        if (response.mtime > fmtime) {
          // cached file older than original
          logger.warn("Scaled image file older than original: " + ii.getFile());
          // read from DHCrud instead
          setupDHCrudInput(uri, logParameter, response, job, repImg);
          if (job.getImgSize().getWidth() == Integer.MAX_VALUE) {
            // reset scale parameters with real image size
            job.setImgSize(repImg.getSize());
            job.prepareScaleParams();
          }
        } else if (response.canSendFile) {
          // check if we can just send the file
          if (!job.isTransformRequired()) {
            /*
             * send the file
             */
            response.imageFile = ii.getFile();
            logger.debug("Sending file as-is (" + (System.currentTimeMillis() - startTime) + "ms): "
                + ii.getFile());
            response.mimeType = ii.getMimetype();
            return response;
          }
        }
      }

      if (response.hasError()) {
        // error while setting up the input
        return response;
      }

      // return info without scaling
      if (dlReq.hasOption(DigilibOption.info) && response.originalWidth != null
          && response.originalHeight != null) {
        logger.debug("Returning info only (" + (System.currentTimeMillis() - startTime) + "ms)");
        return response;
      }

      ByteArrayOutputStream ostream = new ByteArrayOutputStream();
      String outputFormat = job.getOutputMimeType();

      if (job.isTransformRequired()) {
        /*
         * process image
         */
        ImageWorker digilib = new ImageWorker(this.dlConfig, job);
        logger.debug("Scaling with " + digilib);
        DocuImage img = digilib.call();
        logger.debug(Long.toString(System.currentTimeMillis() - startTime) + " ms");

        /*
         * save result
         */
        // write image to buffer
        img.writeImage(outputFormat, ostream);
        response.width = img.getWidth();
        response.height = img.getHeight();
      } else {
        // image from tgcrud seems to be like requested, no processing required
        logger.debug(
            "Sending file from tgcrud as-is (" + (System.currentTimeMillis() - startTime) + "ms)");
        IOUtils.copy(repImg.getInputStream(), ostream);
        response.width = response.originalWidth;
        response.height = response.originalHeight;
      }

      logger.debug("written in " + (System.currentTimeMillis() - startTime) + " ms");
      // set buffer in response
      response.imageData = ostream.toByteArray();
      response.mimeType = outputFormat;

      logger.debug("done.");

    } catch (IOException e) {
      logger.error("IO Exception", e);
      response.errorCode = 500;
      response.errorMsg = "IO Exception!";
    } catch (ImageOpException e) {
      logger.error("ImageOp exception", e);
      response.errorCode = 400;
      response.errorMsg = "Invalid image operation!";
    } finally {
      BaseScaler.runningJobs.decrementAndGet();
    }

    return response;
  }

  /**
   * Read data from TGCrud and set up ImageStream tgCrudInput.
   *
   * @param sessionId
   * @param uri
   * @param logParameter
   * @param response
   * @param metadataHolder
   * @param dataHolder
   * @param job
   * @param tgCrudInput
   * @throws MetadataParseFault
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws AuthFault
   * @throws ProtocolNotImplementedFault
   * @throws IOException
   */
  protected void setupTGCrudInput(final String sessionId, final String uri,
      final String logParameter, ScalerResponse response,
      Holder<MetadataContainerType> metadataHolder, Holder<DataHandler> dataHolder,
      ImageJobDescription job, ImageStream tgCrudInput)
      throws MetadataParseFault, ObjectNotFoundFault, IoFault, AuthFault,
      ProtocolNotImplementedFault, IOException {
    /*
     * TGCrud #READ operation.
     */
    logger.debug("TGCRUD-READ: uri=" + uri);
    this.tgcrud.read(sessionId, logParameter, uri, metadataHolder, dataHolder);
    InputStream istream = dataHolder.value.getInputStream();
    logger.debug("stream=" + istream.toString());
    tgCrudInput.setInputStream(istream);

    /*
     * set up ImageInput with TGCrud stream
     */
    ImageLoaderDocuImage di = new ImageLoaderDocuImage();
    if (response.originalHeight == null || response.originalWidth == null) {
      // reuse reader for stream input
      di.reuseReader = true;
      // identify image size
      logger.debug("Identifying...");
      if (di.identify(tgCrudInput) == null) {
        // unable to identify -- invalid format
        logger.error("Invalid Image Format!");
        response.errorCode = 500;
        response.errorMsg = "Invalid Image Format!";
        return;
      }
      // save original size
      response.originalWidth = di.getWidth();
      response.originalHeight = di.getHeight();
    }
    // re-use DocuImage
    job.setDocuImage(di);
  }

  /**
   * Read data from DHCrud and set up ImageStream tgCrudInput.
   *
   * @param theUri
   * @param logParameter
   * @param response
   * @param job
   * @param dhCrudInput
   * @throws IOException
   */
  protected void setupDHCrudInput(final String theUri, final String logParameter,
      ScalerResponse response, ImageJobDescription job, ImageStream dhCrudInput)
      throws IOException {
    /*
     * DHCrud #READ operation.
     */
    logger.debug("DHCRUD-READ: uri=" + theUri);
    URI uri = URI.create(replaceSlashEscapeChar(theUri));
    Response res = this.dhcrud.read(uri, 0, logParameter);
    int statusCode = res.getStatus();
    String reasonPhrase = res.getStatusInfo().getReasonPhrase();
    if (statusCode != Status.OK.getStatusCode()) {
      logger.error("ERROR loaging image from DH-crud: " + statusCode + " " + reasonPhrase);
      response.errorCode = statusCode;
      response.errorMsg = reasonPhrase;
      return;
    }
    InputStream istream = res.readEntity(InputStream.class);
    logger.debug("stream=" + istream.toString());
    dhCrudInput.setInputStream(istream);
    dhCrudInput.setMimetype(res.getMediaType().toString());
    logger.debug("mimetype=" + dhCrudInput.getMimetype());

    // TODO If we read from DHCRUD, shouldn't we also cache the image? In TG that is not necessary
    // because we already read from local file system!

    /*
     * set up ImageInput with DHCrud stream
     */
    ImageLoaderDocuImage di = new ImageLoaderDocuImage();
    if (response.originalHeight == null || response.originalWidth == null) {
      // reuse reader for stream input
      di.reuseReader = true;
      // identify image size
      logger.debug("Identifying...");
      if (di.identify(dhCrudInput) == null) {
        // unable to identify -- invalid format
        logger.error("Invalid Image Format!");
        response.errorCode = 500;
        response.errorMsg = "Invalid Image Format!";
        return;
      }
      // save original size
      response.originalWidth = di.getWidth();
      response.originalHeight = di.getHeight();
    }
    // re-use DocuImage
    job.setDocuImage(di);
  }

  /**
   * Create a TG-CRUD service in this instance.
   */
  protected void createTgCrudService() {
    logger.debug("TG-crud WSDL location: " + this.tgcrudServerUrl);
    try {
      this.tgcrud = TGCrudClientUtilities.getTgcrud(this.tgcrudServerUrl, true);
    } catch (MalformedURLException e) {
      logger.error(e.getMessage(), e);
    }
    // logger.debug("TG-crud service version: " + tgcrud.getVersion());
  }

  /**
   * Create a DH-CRUD service in this instance.
   */
  protected void createDhCrudService() {
    logger.debug("DH-crud server URL: " + this.dhcrudServerUrl);
    this.dhcrud = JAXRSClientFactory.create(this.dhcrudServerUrl, DHCrudService.class);
    // logger.debug("DH-crud service version: " + dhcrud.getVersion());
  }

  /**
   * Returns the EXIF info field name from the DOM.
   *
   * @param dom
   * @param name
   * @return
   */
  protected Integer getExifInfo(Element dom, String name) {
    try {
      NodeList nl = dom.getElementsByTagNameNS("http://www.w3.org/2003/12/exif/ns#", name);
      Node n = nl.item(0);
      String val = n.getTextContent();
      return new Integer(val);
    } catch (Exception e) {
      logger.debug("Error reading EXIF info:", e);
    }
    return null;
  }

  /**
   * Returns the current digilib configuration (and sets .dlConfig and .dirCache).
   *
   * @param sct
   * @return
   */
  protected DigilibConfiguration getDigilibConfig(ServletContext sct) {
    if (this.dlConfig == null) {
      this.dlConfig = DigilibTextgridConfiguration.getCurrentConfig(sct);
      this.dirCache =
          (DocuDirCache) this.dlConfig.getValue(DigilibTextgridConfiguration.DIR_CACHE_KEY);
    }
    return this.dlConfig;
  }

  /**
   * Use Textgrid URI or DARIAH-DE Handle URI to create cache path from.
   *
   * FIXME Do use the CachingUtils.getCachingPathFromURI() method from crud service! We cannot
   * include crud-common because of class path errors (at the moment!).
   *
   * @param uri
   * @return
   */
  protected static String getPathFromUri(final String uri, boolean usePrescaleSubdirs) {

    String fn = "";

    if (uri.startsWith(TGREP_PREFIX)) {
      // cut "textgrid:"
      String id = uri.substring(9);
      if (usePrescaleSubdirs) {
        // take first two chars
        String dir = id.substring(0, 2);
        fn = dir + File.separatorChar + id;
      } else {
        fn = id;
      }
    } else if (uri.startsWith(DHREP_PREFIX)) {
      String id = replaceOtherEscapeChars(uri);
      // get everything but the last 6 chars "1234-5" as dir.
      if (usePrescaleSubdirs) {
        String dir = id.substring(0, id.length() - 7);
        fn = dir + File.separatorChar + id;
      } else {
        fn = id;
      }
    }

    return fn;
  }

  /**
   * <p>
   * Parse XML using XPath.
   * </p>
   *
   * @param theXMLStream
   * @param thePath
   * @return String value if applicable, null otherwise.
   */
  protected static String getXPathExpressionValue(final String theXML, String thePath) {

    String result = null;

    try {
      DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
      Document document = builder.parse(new ByteArrayInputStream(theXML.getBytes("UTF-8")));
      XPath xpath = XPathFactory.newInstance().newXPath();
      Node n = (Node) xpath.evaluate(thePath, document, XPathConstants.NODE);

      if (n != null) {
        result = n.getTextContent();
      }
    } catch (ParserConfigurationException | SAXException | IOException
        | XPathExpressionException e) {
      // Do nothing, result already is null!
    }

    return result;
  }

  /**
   * @param theResponse
   * @throws IOException
   */
  private static void checkResponse(Response theResponse) throws IOException {

    int statusCode = theResponse.getStatus();
    String reasonPhrase = theResponse.getStatusInfo().getReasonPhrase();

    if (statusCode != Status.OK.getStatusCode()) {
      String message = statusCode + " " + reasonPhrase;
      logger.error(message);
      throw new IOException(message);
    }
  }

  /**
   * check if uri is a baseUri (containing no revision) try to find out latest revision with
   * tgsearch public and tgsearch nonpublic
   *
   * @param uri the uri to check
   * @return uri (with revision, if not yet there)
   */
  protected String tgBaseUriHandling(String uri) {
    if (uri.startsWith(BaseScaler.TGREP_PREFIX) & !uri.contains(".")) {
      Revisions revlist = this.searchClientPublic.infoQuery().listRevisions(uri);
      if (revlist.getRevision().size() == 0) {
        logger.info("TGSEARCH-INFO: no revuri found in public repo, query nonpublic for " + uri);
        revlist = this.searchClientNonPublic.infoQuery().listRevisions(uri);
        if (revlist.getRevision().size() == 0) {
          logger.warn("TGSEARCH-INFO: no revuri found in any repo, for " + uri);
        }
      }
      String revUri = uri + "." + Collections.max(revlist.getRevision());
      logger.info("TGSEARCH-INFO: found " + revUri + " to be latest revision of " + uri);
      return revUri;
    } else {
      return uri;
    }
  }

  /**
   * @return The replaced URI string
   */
  protected static String replaceSlashEscapeChar(final String theURI) {
    return theURI.replace("%2F", "/");
  }

  /**
   * cut "hdl:" & replace "/" and "%2F"
   * 
   * @param theURI
   * @return The replaced URI string
   */
  protected static String replaceOtherEscapeChars(final String theURI) {
    return theURI.substring(4).replace("/", "-").replace("%2F", "-");
  }

}

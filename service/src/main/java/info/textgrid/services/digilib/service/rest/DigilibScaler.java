package info.textgrid.services.digilib.service.rest;

/*
 * #%L digilib-service
 * 
 * %%
 * 
 * Copyright (C) 2011 - 2014 MPIWG Berlin
 * 
 * %%
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>. #L% Author: Robert Casties
 * (casties@mpiwg-berlin.mpg.de)
 */

import info.textgrid.services.digilib.service.utils.BaseScaler;
import info.textgrid.services.digilib.service.utils.ScalerResponse;

import java.util.Date;

import jakarta.servlet.ServletContext;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.MatrixParam;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.CacheControl;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Request;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.ResponseBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.ws.rs.core.UriInfo;
import digilib.conf.DigilibConfiguration;
import digilib.conf.DigilibRequest;
import digilib.io.ImageInput;
import digilib.util.ImageSize;

/**
 * @author casties
 */
@Path("/")
public class DigilibScaler extends BaseScaler {

  protected static final Logger logger = LoggerFactory.getLogger(DigilibScaler.class);

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.services.digilib.service.utils.BaseScaler#getVersion()
   */
  @Override
  public String getVersion() {
    return "1.9.0 ReST/digilib (digilib " + DigilibConfiguration.getClassVersion() + ")";
  }

  /**
   * {scheme}://{server}/rest/digilib/?[fn=xxx]
   * 
   * @param fn
   * @param sid
   * @param logParameter
   * @param request
   * @param uriInfo
   * @param servletContext
   * @return
   * @throws Exception
   */
  @GET
  @Path("/")
  @Produces("image/png")
  public Response getScaledImageParam(@QueryParam("fn") String fn,
      @MatrixParam("sid") @DefaultValue("") String sid,
      @MatrixParam("logParameter") @DefaultValue("") String logParameter, @Context Request request,
      @Context UriInfo uriInfo, @Context ServletContext servletContext) throws Exception {
    return getScaledImage(request, uriInfo, sid, fn, logParameter, servletContext);
  }

  /**
   * {scheme}://{server}/rest/digilib/{uri}?[digilib params]
   * 
   * @param uri
   * @param sid
   * @param logParameter
   * @param request
   * @param uriInfo
   * @param servletContext
   * @return
   * @throws Exception
   */
  @GET
  @Path("/{uri: .+}")
  @Produces("image/png")
  public Response getScaledImagePath(@PathParam("uri") String uri,
      @MatrixParam("sid") @DefaultValue("") String sid,
      @MatrixParam("logParameter") @DefaultValue("") String logParameter, @Context Request request,
      @Context UriInfo uriInfo, @Context ServletContext servletContext) throws Exception {
    return getScaledImage(request, uriInfo, sid, uri, logParameter, servletContext);
  }

  /**
   * @param request
   * @param uriInfo
   * @param sid
   * @param uri
   * @param logParameter
   * @param servletContext
   * @return
   */
  public Response getScaledImage(Request request, UriInfo uriInfo, String sid, String uri,
      String logParameter, ServletContext servletContext) {

    String query = uriInfo.getRequestUri().getQuery();

    // create digilib request
    DigilibRequest dlReq = new DigilibRequest();
    if (query != null) {
      dlReq.setWithParamString(query, "&");
    } else {
      return Response.status(400).entity("Destination width (dw) and/or height (dh) needed!")
          .type(MediaType.TEXT_PLAIN_TYPE).build();
    }
    if (!uri.startsWith(BaseScaler.TGREP_PREFIX) && !uri.startsWith(BaseScaler.DHREP_PREFIX)) {
      return Response.status(404)
          .entity("ID has to be TextGrid URI [" + BaseScaler.TGREP_PREFIX
              + "] or DARIAH-DE Handle [" + BaseScaler.DHREP_PREFIX + "]!")
          .type(MediaType.TEXT_PLAIN_TYPE).build();
    }
    // check if textgrid baseuri and resolve latest revision in that case
    uri = tgBaseUriHandling(uri);
    // take SID from query parameter
    if (sid.isEmpty() && dlReq.hasValue("sid")) {
      sid = dlReq.getAsString("sid");
    }

    // set URI param, remove trailing params (after ?) if existing
    // TODO Maybe we can cope with this using the dlReq things??
    if (uri.indexOf('?') != -1) {
      uri = uri.substring(0, uri.indexOf('?'));
    }

    // configure digilib
    dlConfig = getDigilibConfig(servletContext);

    // get image metadata
    ScalerResponse scalerRes = readMeta(sid, uri, logParameter, null);
    if (scalerRes.hasError()) {
      // send error
      return Response.status(scalerRes.errorCode).entity(scalerRes.errorMsg)
          .type(MediaType.TEXT_PLAIN_TYPE).build();
    }
    // check caching headers
    ResponseBuilder builder = request.evaluatePreconditions(new Date(scalerRes.mtime));
    if (builder != null) {
      // cached version is current
      logger.debug("Returning 'Not Modified'.");
      return builder.build();
    }
    scalerRes.canSendFile = true;
    
    if(scalerRes.imageSet != null ) {
      if(scalerRes.imageSet.get(0).getSize() == null) {
        logger.debug("Size of first image in imageset is NULL, possibly old textgrid image without size metadata.");
        ImageInput originalSizePrescaleImg = scalerRes.imageSet.get(1);
        if(originalSizePrescaleImg != null) {
          ImageSize prescaleImageSize = originalSizePrescaleImg.getSize();
          if(prescaleImageSize != null) {
            logger.debug("Got size from first image in basedir list. Setting for image[0]");
            scalerRes.imageSet.get(0).setSize(prescaleImageSize);
          } else {
            logger.debug("First image in basdir-list has no size???. This will break.");
          }
        } else {
          logger.debug("No image found in basedir list to get size from. This will break.");
        }
      }
    } else {
      logger.debug("No imageSet available. Seems like there are no prescaled Images in basedir list.");
    }

    // do scale
    scalerRes = scale(sid, uri, logParameter, dlReq, scalerRes);
    if (scalerRes.hasError()) {
      // send error
      return Response.status(scalerRes.errorCode).entity(scalerRes.errorMsg)
          .type(MediaType.TEXT_PLAIN_TYPE).build();
    }
    // use image data or file
    Object data = (scalerRes.imageFile != null) ? scalerRes.imageFile : scalerRes.imageData;
    // send response with caching headers
    CacheControl cc = new CacheControl();
    cc.setMaxAge(86400);
    return Response.ok(data).type(scalerRes.mimeType).lastModified(new Date(scalerRes.mtime)).cacheControl(cc)
        .build();
  }

}

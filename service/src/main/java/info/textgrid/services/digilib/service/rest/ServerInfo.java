package info.textgrid.services.digilib.service.rest;

/*
 * #%L digilib-service
 * 
 * %%
 * 
 * Copyright (C) 2011 - 2014 MPIWG Berlin
 * 
 * %%
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * 
 * #L% Author: Jorge Urzua (jurzua@mpiwg-berlin.mpg.de)
 */

import info.textgrid.services.digilib.service.utils.BaseScaler;
import info.textgrid.services.digilib.service.utils.DigilibTextgridConfiguration;
import java.io.InputStream;
import java.lang.management.ManagementFactory;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Properties;
import jakarta.servlet.ServletContext;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import digilib.image.DocuImageFactory;
import digilib.io.DocuDirCache;
import digilib.util.Parameter;

/**
 * @author jurzua
 */
@Path("/")
public class ServerInfo {

  protected static final Logger logger = LoggerFactory.getLogger(ServerInfo.class);

  @GET
  @Path("/")
  @Produces(MediaType.TEXT_HTML)
  public Response getScaledImageParam(@Context ServletContext servletContext) throws Exception {
    StringBuilder sb = new StringBuilder();
    try {
      int mb = 1024 * 1024;

      logger.info(ManagementFactory.getRuntimeMXBean().getName());

      // Getting the runtime reference from system
      Runtime runtime = Runtime.getRuntime();

      logger.debug("##### Heap utilization statistics #####");

      // Print used memory
      logger.debug("Used memory:  " + (runtime.totalMemory() - runtime.freeMemory()) / mb + "mb");

      // Print free memory
      logger.debug("Free memory:  " + runtime.freeMemory() / mb + "mb");

      // Print total available memory
      logger.debug("Total memory: " + runtime.totalMemory() / mb + "mb");

      // Print Maximum available memory
      logger.debug("Max memory:   " + runtime.maxMemory() / mb + "mb\n");

      // collect digilib formats
      StringBuilder fmts = new StringBuilder();
      Iterator<String> dlfs = DocuImageFactory.getInstance().getSupportedFormats();
      for (String f = dlfs.next(); dlfs.hasNext(); f = dlfs.next()) {
        fmts.append(f);
        fmts.append(", ");
      }

      // collect digilib config
      DigilibTextgridConfiguration dlConfig =
          DigilibTextgridConfiguration.getCurrentConfig(servletContext);
      StringBuilder dlcfgs = new StringBuilder();
      for (String k : dlConfig.getParams().keySet()) {
        Parameter p = dlConfig.get(k);
        dlcfgs.append("<p>");
        if (k.equals("basedir-list")) {
          dlcfgs.append(k + " = <br/>");
          String[] bd = (String[]) p.getValue();
          if (bd != null) {
            for (int j = 0; j < bd.length; j++) {
              dlcfgs.append(bd[j] + "<br/>");
            }
          }
        } else {
          dlcfgs.append(k + " = " + p.getAsString());
        }
        dlcfgs.append("</p>");
      }

      // digilib cache stats
      DocuDirCache dirCache = (DocuDirCache) dlConfig.getValue(DigilibTextgridConfiguration.DIR_CACHE_KEY);
      StringBuilder dlcs = new StringBuilder();
      dlcs.append("<p>hits = "+dirCache.getHits()+"</p>");
      dlcs.append("<p>misses = "+dirCache.getMisses()+"</p>");
      dlcs.append("<p>number of files = "+dirCache.getNumFiles()+"</p>");
      
      // collect servlet config
      Properties props = new Properties();
      InputStream s = Thread.currentThread().getContextClassLoader()
          .getResourceAsStream("digilib-service.properties");
      StringBuilder srvcfgs = new StringBuilder();
      if (s != null) {
        props.load(s);
        s.close();
        for (Entry<Object, Object> p : props.entrySet()) {
          srvcfgs.append("<p>");
          srvcfgs.append(p.getKey() + " = " + p.getValue());
          srvcfgs.append("</p>");
        }
      } else {
        logger.warn("Properties file digilib-service.properties not found!");
      }

      sb.append("<!DOCTYPE html><html><body>");
      sb.append("<h1>" + ManagementFactory.getRuntimeMXBean().getName() + "</h1>");
      sb.append("##### Heap utilization statistics #####");
      sb.append("<h3>" + "Used memory: " + (runtime.totalMemory() - runtime.freeMemory()) / mb
          + "mb</h3>");
      sb.append("<h3>" + "Free memory: " + runtime.freeMemory() / mb + "mb</h3>");
      sb.append("<h3>" + "Total memory: " + runtime.totalMemory() / mb + "mb</h3>");
      sb.append("<h3>" + "Max memory: " + runtime.maxMemory() / mb + "mb</h3>");
      sb.append("<h3>" + "Running jobs: " + BaseScaler.runningJobs.get() + "</h3>");
      sb.append("<h3>" + "Supported image types</h3>");
      sb.append("<p>" + fmts + "</p>");
      sb.append("<h3>" + "Digilib configuration</h3>");
      sb.append(dlcfgs);
      sb.append("<h3>" + "Service configuration</h3>");
      sb.append(srvcfgs);
      sb.append("<h3>" + "Digilib cache</h3>");
      sb.append(dlcs);

      sb.append("</body></html>");

    } catch (Exception e) {
      e.printStackTrace();
    }

    return Response.ok(sb.toString()).type(MediaType.TEXT_HTML).build();
  }

}

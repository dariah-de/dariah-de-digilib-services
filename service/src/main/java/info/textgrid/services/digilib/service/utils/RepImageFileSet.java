package info.textgrid.services.digilib.service.utils;

/*
 * #%L
 * digilib-service
 * %%
 * Copyright (C) 2011 - 2020 MPIWG Berlin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 * Author: Robert Casties (casties@mpiwg-berlin.mpg.de)
 */

import java.io.File;
import java.io.IOException;

import digilib.io.FsDirectory;
import digilib.io.ImageCacheStream;
import digilib.io.ImageFileSet;

/**
 * ImageFileSet that holds an additional TG/DHRep image resource.
 * 
 * @author casties
 *
 */
public class RepImageFileSet extends ImageFileSet {

  /** Session-ID last used for the Rep resource */
  protected String sid = null;
  
  /** Modification time of the Rep resource */
  protected long repMtime = 0;
  
  /**
   * Constructor with file and scaleDirs.
   * Required for DocuDirectory's DocuDirent factory.
   * 
   * @param file
   * @param scaleDirs
   */
  public RepImageFileSet(File file, FsDirectory[] scaleDirs) {
    // construct and fill ImageFileSet
    super(file, scaleDirs);
    // add new ImageUri at the front
    try {
      list.add(0, new ImageCacheStream(null, null));
    } catch (IOException e) {
      // this should not happen
    }
  }

  /**
   * @return the sid
   */
  public String getSid() {
    return sid;
  }

  /**
   * @param sid the sid to set
   */
  public void setSid(String sid) {
    this.sid = sid;
  }

  /**
   * @return the resMtime
   */
  public long getRepMtime() {
    return repMtime;
  }

  /**
   * @param resMtime the resMtime to set
   */
  public void setRepMtime(long mtime) {
    this.repMtime = mtime;
  }

}

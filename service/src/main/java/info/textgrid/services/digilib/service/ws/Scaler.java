package info.textgrid.services.digilib.service.ws;

/*
 * #%L digilib-service %% Copyright (C) 2011 - 2014 MPIWG Berlin %% This program is free software:
 * you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>. #L% Author: Robert Casties
 * (casties@mpiwg-berlin.mpg.de)
 */

import info.textgrid.namespaces.services.digilib.service.digilibservice.DigilibServicePortType;
import info.textgrid.services.digilib.service.utils.BaseScaler;
import info.textgrid.services.digilib.service.utils.ScalerResponse;
import jakarta.annotation.Resource;
import jakarta.jws.WebService;
import jakarta.servlet.ServletContext;
import jakarta.xml.ws.Holder;
import jakarta.xml.ws.WebServiceContext;
import jakarta.xml.ws.handler.MessageContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import digilib.conf.DigilibConfiguration;
import digilib.conf.DigilibRequest;

@WebService(
    targetNamespace = "http://textgrid.info/namespaces/services/digilib/service/DigilibService",
    serviceName = "DigilibService", portName = "DigilibServicePort",
    endpointInterface = "info.textgrid.namespaces.services.digilib.service.digilibservice.DigilibServicePortType")
public class Scaler extends BaseScaler implements DigilibServicePortType {

  public String getVersion() {
    return "1.7.1 WS (digilib " + DigilibConfiguration.getClassVersion() + ")";
  }

  protected static final Logger logger = LoggerFactory.getLogger(Scaler.class);

  /** JAX-WS WebServiceContext */
  @Resource
  WebServiceContext wsContext;

  /**
   * Load, scale and return an image.
   * 
   * Loads the image identified by uri and sessionId. Scales the image according to the digilib
   * parameter string in query. Puts the resulting data in the Holders.
   * 
   * @see info.textgrid.namespaces.services.digilib.service.digilibservice.DigilibServicePortType#getScaledImage(java.lang.String,
   *      java.lang.String, java.lang.String, java.lang.String, javax.xml.ws.Holder,
   *      javax.xml.ws.Holder, javax.xml.ws.Holder, javax.xml.ws.Holder, javax.xml.ws.Holder,
   *      javax.xml.ws.Holder, javax.xml.ws.Holder)
   */
  @Override
  public void getScaledImage(String sessionId, String uri, String query, String logParameter,
      Holder<String> mimeType, Holder<byte[]> imageData, Holder<Integer> width,
      Holder<Integer> height, Holder<Integer> originalWidth, Holder<Integer> originalHeight,
      Holder<Integer> originalDpi) {

    logger.debug("getScaledImage: sid=" + sessionId + " uri=" + uri + " query=" + query);

    if (dlConfig == null) {
      // get digilib config
      MessageContext mct = wsContext.getMessageContext();
      ServletContext sct = (ServletContext) mct.get(MessageContext.SERVLET_CONTEXT);
      dlConfig = this.getDigilibConfig(sct);
      if (dlConfig == null) {
        logger.error("No digilib configuration!");
      }
    }
    DigilibRequest dlReq = new DigilibRequest();
    // get operation parameters from query String
    dlReq.setWithParamString(query, "&");
    // do scale
    ScalerResponse response = scale(sessionId, uri, logParameter, dlReq, null);
    if (!response.hasError()) {
      // set buffer in Holder
      imageData.value = response.imageData;
      mimeType.value = response.mimeType;
      width.value = response.width;
      height.value = response.height;
      originalWidth.value = response.originalWidth;
      originalHeight.value = response.originalHeight;
      // original-dpi may not be set
      originalDpi.value = (response.originalDpi != null) ? response.originalDpi : 0;
      logger.debug("done.");
    } else {
      logger.error("Error scaling image: " + response.errorMsg);
      // TODO: what now? throw exception?
    }
  }

}

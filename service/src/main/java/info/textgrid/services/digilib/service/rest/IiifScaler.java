package info.textgrid.services.digilib.service.rest;

/*
 * #%L digilib-service %% Copyright (C) 2011 - 2014 MPIWG Berlin, SUB Göttingen %% This program is
 * free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 * #L% Author: Robert Casties <casties@mpiwg-berlin.mpg.de>, Jorge Urzua
 * <jurzua@mpiwg-berlin.mpg.de>, Ubbo Veentjer <veentjer@sub.uni-goettingen.de>
 */

import java.net.URI;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import jakarta.servlet.ServletContext;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.MatrixParam;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.CacheControl;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Request;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.ResponseBuilder;
import jakarta.ws.rs.core.UriInfo;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import digilib.conf.DigilibConfiguration;
import digilib.conf.DigilibOption;
import digilib.conf.DigilibRequest;
import digilib.io.ImageInput;
import digilib.io.ImageSet;
import digilib.util.ImageSize;

import info.textgrid.services.digilib.service.utils.BaseScaler;
import info.textgrid.services.digilib.service.utils.ScalerResponse;

/**
 *
 */
@Path("/")
@Produces("application/xml")
public class IiifScaler extends BaseScaler {

  protected static final Logger logger = LoggerFactory.getLogger(IiifScaler.class);

  protected String iiifBaseUrl = null; // "http://textgridlab.org/dev/digilib/rest/IIIF"

  /*
   * (non-Javadoc)
   *
   * @see info.textgrid.services.digilib.service.utils.BaseScaler#getVersion()
   */
  @Override
  public String getVersion() {
    return "1.9.0 ReST/IIIF (digilib " + DigilibConfiguration.getClassVersion() + ")";
  }

  /**
   *
   */
  public IiifScaler() {
    super();
    // configure base URL
    iiifBaseUrl = props.getProperty("digilib.service.iiif.baseurl", iiifBaseUrl);
  }

  /**
   * {scheme}://{server}{/prefix}/{identifier}/{region}/{size}/{rotation}/{quality}{.format}
   *
   * @param identifier
   * @param region
   * @param size
   * @param rotation
   * @param quality
   * @param format
   * @param sid
   * @param logParameter
   * @param request
   * @param servletContext
   * @return
   * @throws Exception
   */
  @GET
  @Path("/{identifier}/{region}/{size}/{rotation}/{quality}.{format}")
  public Response getScaledImageWithFormat(@PathParam("identifier") String identifier,
      @PathParam("region") String region, @PathParam("size") String size,
      @PathParam("rotation") String rotation, @PathParam("quality") String quality,
      @PathParam("format") String format, @MatrixParam("sid") @DefaultValue("") String sid,
      @MatrixParam("logParameter") @DefaultValue("") String logParameter, @Context Request request,
      @Context ServletContext servletContext) throws Exception {

    return getScaledImage(request, identifier, region, size, rotation, quality, format, sid,
        logParameter, servletContext);
  }

  /**
   * Image Information Request: http[s]://server/[prefix/]identifier/info.json
   *
   * @param identifier
   * @param sid
   * @param logParameter
   * @param request
   * @param uriInfo
   * @param servletContext
   * @return
   * @throws Exception
   */
  @GET
  @Path("/{identifier}/info.json")
  @Produces("application/ld+json")
  public Response getImgInformationWithJson(@PathParam("identifier") String identifier,
      @MatrixParam("sid") @DefaultValue("") String sid,
      @MatrixParam("logParameter") @DefaultValue("") String logParameter, @Context Request request,
      @Context UriInfo uriInfo, @Context ServletContext servletContext) throws Exception {

    return getImgInformation(request, uriInfo, identifier, sid, logParameter, servletContext);
  }

  /**
   * Image Information Request: http[s]://server/[prefix/]identifier
   *
   * @param identifier
   * @param sid
   * @param logParameter
   * @param request
   * @param uriInfo
   * @param servletContext
   * @return
   * @throws Exception
   */
  @GET
  @Path("/{identifier}")
  @Produces("application/ld+json")
  public Response getImgInformationWithId(@PathParam("identifier") String identifier,
      @MatrixParam("sid") @DefaultValue("") String sid,
      @MatrixParam("logParameter") @DefaultValue("") String logParameter, @Context Request request,
      @Context UriInfo uriInfo, @Context ServletContext servletContext) throws Exception {

    return Response.seeOther(new URI(identifier + "/info.json")).build();
  }

  /**
   * @param request
   * @param uriInfo
   * @param uri
   * @param sid
   * @param logParameter
   * @param servletContext
   * @return
   * @throws JSONException
   */
  public Response getImgInformation(Request request, UriInfo uriInfo, String uri, String sid,
      String logParameter, ServletContext servletContext) throws JSONException {
    // check URI
    if (!uri.startsWith(BaseScaler.TGREP_PREFIX) && !uri.startsWith(BaseScaler.DHREP_PREFIX)) {
      return Response.status(404)
          .entity("ID has to be TextGrid URI [" + BaseScaler.TGREP_PREFIX
              + "] or DARIAH-DE Handle [" + BaseScaler.DHREP_PREFIX + "]!")
          .type(MediaType.TEXT_PLAIN_TYPE).build();
    }
    // check if textgrid baseuri and resolve latest revision in that case
    uri = tgBaseUriHandling(uri);
    // configure digilib
    dlConfig = getDigilibConfig(servletContext);
    // get metadata
    ScalerResponse res = readMeta(sid, uri, logParameter, null);
    if (res.hasError()) {
      return Response.status(res.errorCode).entity(res.errorMsg).type(MediaType.TEXT_PLAIN_TYPE)
          .build();
    }
    // check caching headers
    ResponseBuilder builder = request.evaluatePreconditions(new Date(res.mtime));
    if (builder != null) {
      // cached version is current
      logger.debug("Returning 'Not Modified'.");
      return builder.build();
    }
    // get image size from tgcrud metadata or prescale-dir or image loaded from tgcrud
    ImageSet imgs = res.imageSet;
    if(imgs == null) {
      logger.debug("no exif tg-meta and no prescale found, getting size from original image: " + uri);
      // no images found in basedir-list
      // create DigilibRequest and load image from tgcrud
      DigilibRequest dlReq = new DigilibRequest();
      dlReq.setWithIiifImageParams(uri, "full", "100,", null, null, null);
      dlReq.getOptions().setOption(DigilibOption.info);
      res = scale(sid, uri, logParameter, dlReq, res);
      if (res.hasError()) {
        return Response.status(res.errorCode).entity(res.errorMsg).type(MediaType.TEXT_PLAIN_TYPE)
            .build();
      }
    }

    int originalWidth;
    int originalHeight;
    if(res.originalWidth != null) {
      // use size from tgcrud exif metadata or prescaling
      originalWidth =  res.originalWidth;
      originalHeight = res.originalHeight;
    } else {
      logger.debug("no exif tg-meta, getting size from first image in basedir list: " + uri);
      // old textgrid images do not have exif info in metadata.
      // fallback to the size of the 2nd image (the first from
      // the basedir-list) which should be in original size
      originalWidth = imgs.get(1).getSize().getWidth();
      originalHeight = imgs.get(1).getSize().getHeight();
    }

    String url = iiifBaseUrl;
    if (url == null) {
      // try to use current URL as base URL
      url = uriInfo.getBaseUri().toString();
    }
    JSONObject json = new JSONObject();
    // IIIF image api 2.0
    json.put("@context", "http://iiif.io/api/image/2/context.json");
    json.put("@id", url + "/" + uri);
    json.put("protocol", "http://iiif.io/api/image");
    // level 2 profile
    JSONArray profile = new JSONArray();
    profile.put("http://iiif.io/api/image/2/level2.json");
    JSONObject options = new JSONObject();
    options.put("formats", new JSONArray(Arrays.asList("jpg", "png")));
    options.put("qualities", new JSONArray(Arrays.asList("color", "gray")));
    options.put("supports",
        new JSONArray(Arrays.asList("mirroring", "rotationArbitrary", "sizeAboveFull")));
    profile.put(options);
    json.put("profile", profile);
    json.put("width", originalWidth);
    json.put("height", originalHeight);
    // add sizes of prescaled images
    if (imgs != null && imgs.size() > 1) {
      HashMap<Integer,SortedSet<Integer>> tilesMap = new HashMap<Integer,SortedSet<Integer>>();
      // a map for sizes to make sure every size is only mentioned once for every width
      SortedMap<Integer,JSONObject> sizesMap = new TreeMap<Integer,JSONObject>();
      int numImgs = imgs.size();
      for (int i = numImgs - 1; i >= 1; --i) {
        ImageInput ii = imgs.get(i);
        if(ii.getTileSize() != null) { // is it a tiled image?
          int tileSize = ii.getTileSize().getWidth();
          int scaleFactor = Math.round((float) originalWidth / (float) ii.getSize().getWidth());
          // create new entry for each tile size and add its scalefactors
          tilesMap.computeIfAbsent(tileSize, scaleFactors -> new TreeSet<Integer>())
            .add(scaleFactor);
        }
        ImageSize is = ii.getSize();
        JSONObject size = new JSONObject();
        size.put("width", is.getWidth());
        size.put("height", is.getHeight());
        sizesMap.put(is.getWidth(), size);
      }

      json.put("sizes", sizesMap.values());

      JSONArray tiles = new JSONArray();
      tilesMap.forEach((size, scaleFactors) -> {
        try {
          tiles.put(new JSONObject().put("width", size).put("height", size).put("scaleFactors", scaleFactors));
        } catch(JSONException e) {
          logger.error("Problem creating new JSONObject", e);
        }
      });
      json.put("tiles", tiles);
    }

    // send response with caching headers
    CacheControl cc = new CacheControl();
    cc.setMaxAge(86400);
    return Response.ok(json.toString(2).replaceAll("\\\\/", "/")).lastModified(new Date(res.mtime))
        .cacheControl(cc).build();
  }

  /**
   * @param request
   * @param uri
   * @param region
   * @param size
   * @param rotation
   * @param quality
   * @param format
   * @param sid
   * @param logParameter
   * @param servletContext
   * @return
   * @throws Exception
   */
  public Response getScaledImage(Request request, String uri, String region, String size,
      String rotation, String quality, String format, String sid, String logParameter,
      ServletContext servletContext) throws Exception {

    if (!uri.startsWith(BaseScaler.TGREP_PREFIX) && !uri.startsWith(BaseScaler.DHREP_PREFIX)) {
      return Response.status(404)
          .entity("ID has to be TextGrid URI [" + BaseScaler.TGREP_PREFIX
              + "] or DARIAH-DE Handle [" + BaseScaler.DHREP_PREFIX + "]!")
          .type(MediaType.TEXT_PLAIN_TYPE).build();
    }
    // check if textgrid baseuri and resolve latest revision in that case
    uri = tgBaseUriHandling(uri);
    // configure digilib
    dlConfig = getDigilibConfig(servletContext);
    // create digilib request
    DigilibRequest dlReq = new DigilibRequest();
    if (!dlReq.setWithIiifImageParams(uri, region, size, rotation, quality, format)) {
      // there was a problem with the request
      return Response.status(400).entity(dlReq.errorMessage).type(MediaType.TEXT_PLAIN_TYPE)
          .build();
    } ;
    // get image metadata
    ScalerResponse scalerRes = readMeta(sid, uri, logParameter, null);
    if (scalerRes.hasError()) {
      // send error
      return Response.status(scalerRes.errorCode).entity(scalerRes.errorMsg)
          .type(MediaType.TEXT_PLAIN_TYPE).build();
    }
    // check caching headers
    ResponseBuilder builder = request.evaluatePreconditions(new Date(scalerRes.mtime));
    if (builder != null) {
      // cached version is current
      logger.debug("Returning 'Not Modified'.");
      return builder.build();
    }
    // yes, we can send files
    scalerRes.canSendFile = true;

    if(scalerRes.imageSet != null ) {
      if(scalerRes.imageSet.get(0).getSize() == null) {
        logger.debug("Size of first image in imageset is NULL, possibly old textgrid image without size metadata.");
        ImageInput originalSizePrescaleImg = scalerRes.imageSet.get(1);
        if(originalSizePrescaleImg != null) {
          ImageSize prescaleImageSize = originalSizePrescaleImg.getSize();
          if(prescaleImageSize != null) {
            logger.debug("Got size from first image in basedir list. Setting for image[0]");
            scalerRes.imageSet.get(0).setSize(prescaleImageSize);
          } else {
            logger.debug("First image in basdir-list has no size???. This will break.");
          }
        } else {
          logger.debug("No image found in basedir list to get size from. This will break.");
        }
      }
    } else {
      logger.debug("No imageSet available. Seems like there are no prescaled Images in basedir list.");
    }

    // do scale
    scalerRes = scale(sid, uri, logParameter, dlReq, scalerRes);
    if (scalerRes.errorCode != null) {
      return Response.status(scalerRes.errorCode).entity(scalerRes.errorMsg)
          .type(MediaType.TEXT_PLAIN_TYPE).build();
    }
    // use image data or file
    Object data = (scalerRes.imageFile != null) ? scalerRes.imageFile : scalerRes.imageData;
    // send response with caching headers
    CacheControl cc = new CacheControl();
    cc.setMaxAge(86400);
    return Response.ok(data).type(scalerRes.mimeType).lastModified(new Date(scalerRes.mtime)).cacheControl(cc)
        .build();
  }

}

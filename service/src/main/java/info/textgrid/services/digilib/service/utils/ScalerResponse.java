package info.textgrid.services.digilib.service.utils;

/*
 * #%L
 * digilib-service
 * %%
 * Copyright (C) 2011 - 2014 MPIWG Berlin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 * Author: Robert Casties (casties@mpiwg-berlin.mpg.de)
 */

import java.io.File;

import digilib.io.ImageSet;

/**
 * Object carrying information about the scaling operation.
 *   
 * @author casties
 *
 */
public class ScalerResponse {
	
    /** mime-type of image data */
    public String mimeType;
    
    /** modification timestamp of original image */
    public long mtime;
    
    /** textgrid URI (from metadata) */
    public String cacheLocation;
    
    /** array of encoded image data */
    public byte[] imageData;
    /** image as File (instead of imageData) */
    public File imageFile;
    
    /** width of encoded image */
    public Integer width;
    /** width of encoded image */
    public Integer height;
    
    /** width of original image */
    public Integer originalWidth;
    /** height of original image */
    public Integer originalHeight;
    /** dpi of original image */
    public Integer originalDpi;
    
    /** ImageSet of available ImageInputs */
    public ImageSet imageSet;
    
    /** error code during operation */
    public Integer errorCode;
    /** error message during operation */
    public String errorMsg;
    
    /** can the scaler return imageFile instead of imageData */
    public boolean canSendFile = false;
    
    public boolean hasError() {
        return (errorCode != null);
    }
}

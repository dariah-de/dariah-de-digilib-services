.. tg-digilib documentation master file, created by
   sphinx-quickstart on Thu May 21 12:41:35 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


TextGrid Digilib
================

TG-digilib is a version of digilib (see digilib_), which works with textgrid-uris and is integrated with the textgrid repository.


Configuration
-------------

TG-digilib is configured and setup by puppet.


Dependencies
------------

* wildfly, for prescaling images
* textgrid message beans


API Documentation
-----------------

Digilib supports the digilib_scaler_api_ and the iiif_image_api_ for retrieving textgrid image objects.

Digilib scaler API example **request**::

    https://textgridlab.org/1.0/digilib/rest/digilib/textgrid:wtpr.0?dh=100&dw=100

**Response**: 100px preview image

.. image:: pics/textgrid_wtpr_0.jpg
    :target: https://textgridlab.org/1.0/digilib/rest/digilib/textgrid:wtpr.0?dh=100&dw=100

IIIF Image API example **request**::

    https://textgridlab.org/1.0/digilib/rest/IIIF/textgrid:225j0.0/full/,100/0/native.jpg

**Response**: 100px preview image

.. image:: pics/textgrid_225j0_0.jpg
    :target: https://textgridlab.org/1.0/digilib/rest/IIIF/textgrid:225j0.0/full/,100/0/native.jpg

For a complete reference see IIIF Image API. It is possible to extent the URI with
a session id to access non-public content. Adjust the path **like the following**::

    https://textgridlab.org/1.0/digilib/rest/IIIF/textgrid:225j0.0;sid=[YOUR_PRIVATE_SID]/full/,100/0/native.jpg


Sources
-------

See tg-digilib_sources_


Bugtracking
-----------

See tg-digilib_bugtracking_


License
-------

See LICENCE_

.. _LICENCE: https://gitlab.gwdg.de/dariah-de/dariah-de-digilib-services/-/blob/main/LICENSE.txt
.. _tg-digilib_sources: https://gitlab.gwdg.de/dariah-de/dariah-de-digilib-services
.. _tg-digilib_bugtracking: https://gitlab.gwdg.de/dariah-de/dariah-de-digilib-services/-/issues
.. _digilib: http://digilib.sourceforge.net/
.. _digilib_scaler_api: http://digilib.sourceforge.net/scaler-api.html
.. _iiif_image_api: http://iiif.io/api/image/2.1/

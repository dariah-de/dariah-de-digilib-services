package info.textgrid.services.digilib.clients;

/*
 * #%L
 * digilib-client
 * %%
 * Copyright (C) 2011 - 2014 MPIWG Berlin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 * Author: Robert Casties <casties@mpiwg-berlin.mpg.de>
 */

import jakarta.xml.ws.Holder;

/**
 * Class holding data returned by the digilib service.
 * 
 * @author casties
 *
 */
public class DigilibImageData {

    public Holder<String> mimeType = new Holder<String>();
    public Holder<byte[]> imageData = new Holder<byte[]>();
    public Holder<Integer> width = new Holder<Integer>();
    public Holder<Integer> height = new Holder<Integer>();
    public Holder<Integer> originalWidth = new Holder<Integer>();
    public Holder<Integer> originalHeight = new Holder<Integer>();
    public Holder<Integer> originalDpi = new Holder<Integer>();

    /**
     * Returns the mime-type of the scaled image data.
     * 
     * @return the mimeType
     */
    public String getMimeType() {
        return mimeType.value;
    }
    /**
     * Returns raw image data of the scaled image in the format indicated by mimeType as byte array.
     * 
     * @return the imageData
     */
    public byte[] getImageData() {
        return imageData.value;
    }
    /**
     * Returns the width of the scaled image.
     * 
     * @return the width
     */
    public int getWidth() {
        return width.value;
    }
    /**
     * Returns the height of the scaled image.
     * 
     * @return the height
     */
    public int getHeight() {
        return height.value;
    }
    /**
     * Returns the width of the original image on the server.
     * 
     * @return the originalWidth
     */
    public int getOriginalWidth() {
        return originalWidth.value;
    }
    /**
     * Returns the height of the original image on the server.
     * 
     * @return the originalHeight
     */
    public int getOriginalHeight() {
        return originalHeight.value;
    }
    /**
     * Returns the dpi of the original image on the server.
     *      
     * @return the originalDpi
     */
    public int getOriginalDpi() {
        return originalDpi.value;
    }

}

/**
 * Utility methods for interacting with the Textgrid-Digilib-Service.
 */
package info.textgrid.services.digilib.clients;

/*
 * #%L
 * digilib-client
 * %%
 * Copyright (C) 2011 - 2014 MPIWG Berlin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 * Author: Robert Casties <casties@mpiwg-berlin.mpg.de>
 */

import info.textgrid.middleware.confclient.ConfservClient;
import info.textgrid.namespaces.services.digilib.service.digilibservice.DigilibService;
import info.textgrid.namespaces.services.digilib.service.digilibservice.DigilibServicePortType;

import java.net.URL;
import java.util.HashMap;

import jakarta.xml.ws.Holder;

/**
 * @author casties
 * 
 */
public class DigilibServiceUtils {

    public static final String TG_CONF_URL = "https://textgridlab.org/1.0/confserv";
    public static final String TGCONF_DIGILIB_KEY = "digilib";

    public static String getServiceUrl() {
        String uri = null;
        // get CRUD url from conf service
        ConfservClient confservClient = new ConfservClient(TG_CONF_URL);
        HashMap<String, String> tgconf;
        try {
            tgconf = confservClient.getAll();
            uri = tgconf.get(DigilibServiceUtils.TGCONF_DIGILIB_KEY);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (uri == null || uri == "") {
            // set default URL
            uri = "http://digilibtextgridservice.mpiwg-berlin.mpg.de/digilibservice/service?wsdl";
            //System.out.println("  using default TGCrud URL: " + uri);
        }
        if (uri.endsWith("service")) {
            uri += "?wsdl";
        }
        //System.out.println("  got digilib service URL: " + uri);
        return uri;
    }

    /**
     * Calls digilib service and returns scaled image data in DigilibImageData object.
     * 
     * digilib query parameters as in <http://developer.berlios.de/docman/display_doc.php?docid=106&group_id=251>
     * 
     * @param sid
     * @param uri
     * @param query
     * @param serviceUrl
     * @return
     */
    public static DigilibImageData getScaledImage(String sid, String uri, String query, URL serviceUrl) {
        DigilibService service = new DigilibService(serviceUrl);
        DigilibServicePortType port = service.getDigilibServicePort();
        String logParam = null;
        // Holder for return data
        DigilibImageData img = new DigilibImageData();
        // call service -- fills Holder
        port.getScaledImage(sid, uri, query, logParam, img.mimeType, img.imageData, img.width, img.height, img.originalWidth, img.originalHeight,
                img.originalDpi);
        return img;
    }

    /**
     * Calls digilib service and returns scaled image data in Holder objects.
     * 
     * digilib query parameters as in <http://developer.berlios.de/docman/display_doc.php?docid=106&group_id=251>
     * 
     * @param sid
     * @param uri
     * @param query
     * @param serviceUrl
     * @param mimeType
     * @param imageData
     * @param width
     * @param height
     * @param originalWidth
     * @param originalHeight
     * @param originalDpi
     * @return
     */
    public static byte[] getScaledImage(String sid, String uri, String query, URL serviceUrl, Holder<String> mimeType,
            Holder<byte[]> imageData, Holder<Integer> width, Holder<Integer> height, Holder<Integer> originalWidth,
            Holder<Integer> originalHeight, Holder<Integer> originalDpi) {
        DigilibService service = new DigilibService(serviceUrl);
        DigilibServicePortType port = service.getDigilibServicePort();
        String logParam = null;
        // call service -- fills Holder
        port.getScaledImage(sid, uri, query, logParam, mimeType, imageData, width, height, originalWidth, originalHeight,
                originalDpi);
        return imageData.value;
    }
    
    /**
     * Returns the version of the digilib service.
     * 
     * @param serviceUrl
     * @return
     */
    public static String getVersion(URL serviceUrl) {
        DigilibService service = new DigilibService(serviceUrl);
        DigilibServicePortType port = service.getDigilibServicePort();
        // call service -- fills Holder
        String version = port.getVersion();
        return version;
    }
}

package info.textgrid.services.digilib.clients.cli;

/*
 * #%L
 * digilib-client
 * %%
 * Copyright (C) 2011 - 2014 MPIWG Berlin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 * Author: Robert Casties <casties@mpiwg-berlin.mpg.de>
 */

import info.textgrid.services.digilib.clients.DigilibServiceUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;

import jakarta.xml.ws.Holder;

/**
 * digilib service test class with command line interface.
 * @author casties
 * 
 */
public class DigilibCli {

    /**
     * @param args
     */
    public static void main(String[] args) {
        if (args.length < 4) {
            System.err.println("use: DigilibCli SID URI QUERY FILENAME [SERVERURL]");
            System.exit(1);
        }
        long start = System.currentTimeMillis();
        String sid = args[0];
        String uri = args[1];
        String query = args[2];
        String filename = args[3];
        URL serverUrl = null;
        try {
            if (args.length > 4) {
                serverUrl = new URL(args[4]);
            } else {
                String serviceUrl = DigilibServiceUtils.getServiceUrl();
                serverUrl = new URL(serviceUrl);
            }
        } catch (MalformedURLException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        System.out.println("service at " + serverUrl + " ("+(System.currentTimeMillis() - start)+"ms)");
        System.out.println("service version: " + DigilibServiceUtils.getVersion(serverUrl) + " ("+(System.currentTimeMillis() - start)+"ms)");
        
        // call service
        byte[] imageData = getScaledImage(sid, uri, query, serverUrl);
        // try to copy to file
        System.out.println("writing " + filename + " ("+(System.currentTimeMillis() - start)+"ms)");
        File f = new File(filename);
        try {
            OutputStream ostream = new FileOutputStream(f);
            ostream.write(imageData);
            ostream.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        System.out.println("Done in "+(System.currentTimeMillis()-start)+"ms");
    }
    
    public static byte[] getScaledImage(String sid, String uri, String query, URL serverUrl) {
        // Holder for return data
        Holder<String> mimeType = new Holder<String>();
        Holder<byte[]> imageData = new Holder<byte[]>();
        Holder<Integer> width = new Holder<Integer>();
        Holder<Integer> height = new Holder<Integer>();
        Holder<Integer> originalWidth = new Holder<Integer>();
        Holder<Integer> originalHeight = new Holder<Integer>();
        Holder<Integer> originalDpi = new Holder<Integer>();
        // call service -- fills Holder
        DigilibServiceUtils.getScaledImage(sid, uri, query, serverUrl, mimeType, imageData, width, height, originalWidth,
                originalHeight, originalDpi);
        // print values
        System.out.println("mimeType=" + mimeType.value);
        System.out.println("imageData=" + imageData.value);
        System.out.println("width=" + width.value);
        System.out.println("height=" + height.value);
        System.out.println("originalWidth=" + originalWidth.value);
        System.out.println("originalHight=" + originalHeight.value);
        System.out.println("originalDpi=" + originalDpi.value);
        return imageData.value;
    }

}
